import sys
import os
import re
import hashlib
import shutil
import json
import argparse

parser = argparse.ArgumentParser(description='Direction for PlanetScope data')
parser.add_argument('indir', type=str, help='Input dir for PS data ')
parser.add_argument('folder', type=str, help='Input folder name for PS data')
args = parser.parse_args()

absolute_path = args.indir
dataFolder = args.folder

data_path = os.path.join(absolute_path, dataFolder)

# Проверка содержания XML и TIF файлов в каталоге
tifFile_counter = 0
xmlFile_counter = 0
tifFile = None
xmlFile = None

files = os.listdir(absolute_path)
for i in os.listdir(absolute_path):
    if os.path.isfile(os.path.join(absolute_path, i)):
        if i.endswith(".tif"):
            tifFile = os.path.join(absolute_path, i)
            tifFile_counter += 1
        if i.endswith("xml"):
            xmlFile = os.path.join(absolute_path, i)
            xmlFile_counter += 1

if tifFile_counter == 0 or xmlFile_counter == 0:
    print("Ошибка! В каталоге отсутвует XML или TIF файл")
    sys.exit()
elif tifFile_counter > 1 or xmlFile_counter > 1:
    print("Ошибка! В каталоге должно быть по одному XML ии TIF файлу")
    sys.exit()
else:
    print("XML и TIF файлы успешно добавлены")

# Создание выбранной директории, добавление файлов seance и source
try:
    os.mkdir(data_path)
    print("Директория {} успешно создана".format(dataFolder))
except:
    print("Директория {} уже создана".format(dataFolder))

seanceFile = os.path.join(data_path, "_seance.inf")
seanceOpenFile = open(seanceFile, "w")

sourceFile = os.path.join(data_path, "1_source_0.inf")
sourceOpenFile = open(sourceFile, "w")
copyTifFile = os.path.join(data_path, "1_source_0.tif")

if tifFile != None:
    shutil.copy2(tifFile, copyTifFile)
else:
    print("tif Файл не найден")
    sys.exit()

# md5-Hash
with open(tifFile,"rb") as BinaryFile:
    bytes = BinaryFile.read() # read file as bytes
    readable_hash = hashlib.md5(bytes).hexdigest()

# Исследование XML файла
dateStr = None
timeStr = None
contour_wkt = ""

regex_datetime = r"<eop:acquisitionDate>(.*?)T(\d+:\d+:\d+).*<\/eop:acquisitionDate>"

with open(xmlFile, 'r') as xmlFile:
    textStr = xmlFile.read().replace('\n', '')

# Достаем координаты
matches_datetime = re.finditer(regex_datetime, textStr, re.MULTILINE)
coordinatesList = re.findall(r"<gml:coordinates>(.*)<\/gml:coordinates>", textStr)[0]
coordinatesPoint = coordinatesList.split(" ")

for i in range(len(coordinatesPoint)):
    point = str(coordinatesPoint[i]).split(",")
    contour_wkt += point[0] + " " + point[1] + ","

contour_wkt += str(coordinatesPoint[0]).split(",")[0] + " " + str(coordinatesPoint[0]).split(",")[1]

# Достаем datetime
for matchNum, match in enumerate(matches_datetime, start=1):
    dateStr = str(match.groups()[0])
    timeStr = str(match.groups()[1])

# Запись в seance файл
seanceOpenFile.write("[Common]\n"
                     "Satellite = DOVE\n"
                     "Device = PlanetScope\n"
                     "Date = {} \n"
                     "Time = {} \n"
                     "Station = SMIS\n"
                     "\n"
                     "[fragment_1]\n"
                     "contour_wkt = POLYGON(({}))\n".format(dateStr, timeStr, contour_wkt))

seanceOpenFile.close()

# Добавление значений в Source файл

sun_azimuth = re.findall(r"<opt:illuminationAzimuthAngle uom=\"deg\">(\S+)</opt:illuminationAzimuthAngle>", textStr)[0]
sun_elevation = re.findall(r"<opt:illuminationElevationAngle uom=\"deg\">(\S+)</opt:illuminationElevationAngle>", textStr)[0]
eop_identifier = re.findall(r"<eop:identifier>(\S+)</eop:identifier>", textStr)[0]

ps_projection = re.findall(r"<ps:projection>(.*)/.*</ps:projection>", textStr)[0]
ps_projectionZone = re.findall(r"<ps:projectionZone>(\d+)</ps:projectionZone", textStr)[0]

calib_scale_str = ""
calib_scale_info = re.findall(r"<ps:bandNumber>(\d+)</ps:bandNumber>", textStr)
mult = re.findall(r"<ps:radiometricScaleFactor>(\S+)</ps:radiometricScaleFactor>", textStr)
mult_rad = re.findall(r"<ps:reflectanceCoefficient>(\S+)</ps:reflectanceCoefficient>", textStr)

outer_dict = {}
for i in range(len(calib_scale_info)):
    mult_rad_value = "{:.16f}".format(float(mult_rad[i]))
    inner_dict = {"add_rad": 0, "mult_rad": mult_rad_value, "mult": float(mult[i]), "add": 0}
    outer_dict[calib_scale_info[i]] = inner_dict
    calib_scale_str += str(outer_dict)

hist_dict = {
    "sun_azimuth": float(sun_azimuth),
    "sun_elevation": float(sun_elevation),
    "datatake_identifier": eop_identifier,
    "proj4": "+proj=utm +zone={} +datum={} + units=m+no_defs".format(ps_projectionZone, "".join(str(ps_projection).split())),
    "calib_scale_info": outer_dict
}

sourceOpenFile.write("[Common]\n"
                     "md5 = {}\n"
                     "hist = {}".format(readable_hash, json.dumps(hist_dict)))

print("Данные успешно загружены")
sourceOpenFile.close()
xmlFile.close()

